Установка
=========

```
#!bash
git clone https://bitbucket.org/0x255/.vim.git $HOME/.vim

ln -s .vim/_vimrc $HOME/.vimrc

ln -s .vim ~/.config/nvim
```
