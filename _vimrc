" vim: foldmethod=marker foldcolumn=4 foldlevel=0 textwidth=120 fileencoding=utf-8
" Maintrainer: 0x255
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 БАЗОВЫЕ НАСТРОЙКИ                                            "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

set nocompatible                    " Отключаем режим совместимости с vi
set ttyfast                         " У нас ведь быстрый терминал?)
set lazyredraw                        " не отрисовываем экран при вызове макросов

set hidden                          " Не выгружать буфер, когда переключаемся на другой.
                                    " Это позволяет редактировать несколько файлов в один
                                    " и тот же момент без необходимости сохранения
                                    " каждый раз когда переключаешься между ними

" <c-n> и <c-p> в коммандном режиме основываются на введенной фразе
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>

" для .vimrc по K открываем справку для слова под курсором
autocmd FileType vim nnoremap <silent> <buffer> K :help <c-r><c-w><CR>
"autocmd FileType vim setlocal keywordprg=:help

set confirm                         " Использовать диалоги вместо сообщений об ошибках
set splitbelow                      " Новое окно появляется внизу
set splitright                      "                       cправа

set autoread                        " Включение автоматического перечитывания файла при изменении

set nobackup                        " Отключаем надоедливые бэкапы (файлы с ~)
set noswapfile                      " Oтключаем создание swap файлов
set undofile                        " Храним 'отмены'
set backupdir=~/.vim_local/bkp       " Куда сохранять бэкапы
set directory=~/.vim_local/swp      "                swap-файлы
set undodir=~/.vim_local/undo       "                историю правок (отмены)

" Создать эти каталоги автоматом, если они не существуют
if !isdirectory(expand(&backupdir))
    call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
    call mkdir(expand(&directory), "p")
endif
if !isdirectory(expand(&undodir))
    call mkdir(expand(&undodir), "p")
endif

" штатный механизм подгрузки доп. конфига.
" файлы .vimrc/.exrc ищутся от текущей директории до корня
" удобно положить такой файл на первый уровень каталого проекта, задав в нём специфичные для проекта настройки
set exrc

" если указали несколько файлов-аргументов, то открываем их в новых табах
autocmd vimenter * if !argc() | tab sball | endif

" испольуем более стойкое шифрование
if has("cryptv")
    set cryptmethod=blowfish
endif

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 ОТОБРАЖЕНИЕ                                                  "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

set nowrap                          " Отменяем перенос строк
set linebreak                       " Перенос слов целиком
" показываем невидимые символы
set listchars=eol:↲,tab:▹‧,trail:_,extends:▸,precedes:◂
"set listchars=eol:¶,tab:»\ ,trail:·,extends:→,precedes:←,nbsp:×

set number                          " Включаем нумерацию строк
set relativenumber                  " Относительная нумерация
set ruler                           " Показывать положение курсора всё время.

set cursorline                      " Подсветка текущей строки
"let &t_SI="\<Esc>]12;green\x7"     " Цвет курсора в режиме редактирования
"let &t_EI="\<Esc>]12;blue\x7"      " Цвет курсора в режиме навигации

set scrolloff=5                     " Количество строк вокруг курсора (верт.)
set sidescrolloff=5                 " Количество строк вокруг курсора (гориз.)
set scrolljump=5

set hlsearch                        " Выделять все результаты поиска
set incsearch                       " Поиск во время набора текста
set ignorecase                      " Игнорируем регистр при поиске
set smartcase                       " Но если поисковая строка содержит вeрхний

" Настройки табуляторов
set tabstop=4                       " Размер табуляции
set softtabstop=4                   " 4 пробела в табе
set shiftwidth=4                    " Размер сдвига при нажатии на клавиши < и >
set expandtab                       " Преобразование Таба в пробелы

                                    " Настройка свертки
                                    "   * indent, когда сворачивание происходит по отступам;
                                    "   * syntax,                    определяется файлом подсветки синтаксиса;
                                    "   * manual,                    определяется вручную;
                                    "   * marker, для сворачивания используются специальные маркеры (в комментарии {{{ и }}})
                                    "   * diff,                                 текст, который не изменился
set foldmethod=indent               " Фолдинг по отсупам
set foldlevel=25                    " Уровень открытых фолдингов по умолчанию
"set autochdir                      " Автоматическое открытие сверток при заходе в них

set showmatch                       " Показывать совпадающую скобку

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 РЕДАКТОР                                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

" ширина всех текстовых файлов 130 символов
autocmd FileType text setlocal textwidth=120

"Перед сохранением вырезаем пробелы на концах строк
autocmd BufWritePre * normal m`;%s/\s\+$//e ``

" что может удалить backspace
set backspace=indent,eol,start whichwrap+=<,>,[,]

set smartindent                     " Включаем умные отспупы например, автоотступ после {
set autoindent                      " Наследовать отступы от предыдущей строки

set fo+=cr                          " Продолжаем комментарий при нажатии Enter в режиме вставки
                                    " автоматом разбивая строки по длине textwidth

set complete=""                     " Места дополения введённых фраз:
set complete+=.                     " Из текущего буфера
set complete+=k                     "    словаря
set complete+=b                     "    других открытых буферов
set complete+=t                     "    тегов

set completeopt-=preview
set completeopt+=longest

" spelling
set spelllang=ru_yo,en_us

menu spell.off                ;setlocal nospell
menu spell.ru                 ;setlocal spell spelllang=ru_yo
menu spell.en                 ;setlocal spell spelllang=en_us
menu spell.ru_en              ;setlocal spell spelllang=ru_yo,en_us

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 UI                                                          "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

language messages POSIX             " Локаль вывода сообщений

set title                           " показывать имя файла в заголовке терминала
set titlestring=%t%(\ %m%)%(\ %r%)%(\ %h%)%(\ %w%)%(\ (%{expand(\"%:p:~:h\")})%)\ -\ VIM

set novisualbell                    " не мигать!
set t_vb=                           " не пишать!

set showtabline=2                   " Показывать строку вкладок всегда

set fillchars=stl:\ ,stlnc:\ ,vert:│ " убирает отступы | при отрисовке региона

" Формат строки состояния
set showcmd                         " Показывать незавершённые команды в статусбаре
set history=250                     " Длина буффера комманд
set undolevels=1000                 " Размер истории для отмены правок
set statusline=%<%f%h%m%r%=format=%{&fileformat}\ file=%{&fileencoding}\ enc=%{&encoding}\ %b\ 0x%B\ %l,%c%V\ %P
set laststatus=2                    " Показыать строку статуса для каждого окна

set shortmess=I                     " Укорачиваем вывод в строке состояния... (atI)

set wildmenu                        " Более удобное дополнение командной строки
set wildcharm=<Tab>
set wildignorecase
set wildmode=list:longest,full      " Автозавершение имен файлов в стиле bash
set wildignore=*.o,*.so,*.pyc,*~,*\.git,*/__pycache__    " убираем из автокомплита,

"{{{ GUI
if has("gui_running")
                                    " Скрыть меню, тулбар и скролы
    set guioptions-=m               " меню
    set guioptions-=M               " не загружаем '$VIMRUNTIME/menu.vim'
    set guioptions-=e               " вкладки GUI делаем их как в консоли
    set guioptions-=T               " тулбар
    set guioptions-=r               " полосы прокрутки справа
    set guioptions-=L               " полосы прокрутки слева
    set guioptions-=T               " полосы прокрутки слева
    set guioptions+=c               " использовать консольный интерефейс в модальных окнах

    set guifont=Terminus\ 9

    set lines=40 columns=80

    " таскаем табы по CTR+стрелки
    nnoremap <silent> <C-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
    nnoremap <silent> <C-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>

endif

if has("gui_running") || &term== "rxvt" || &term=="rxvt-unicode-256color" || &term== "screen-256-color"
    set t_Co=256
endif

if &t_Co < 256
    colorscheme slate
else
    colorscheme lucius
    set background=dark
endif
"}}}

if &readonly
    set nofoldenable                " Не сворачивать блоки текста
    set foldcolumn=5                " Делаем отступ слева для более удобного чтения текста
endif

if &diff
    set diffopt+=iwhite             " Игнорируем пробелы при diff
    set scrollbind                  " Синхронно листаем файлы
    set scrollopt+=hor              " Синхронизация при горизонтальном скроле
endif

" Меню выбора кодировки текста
menu cp.r.utf8 ;e ++enc=utf8<CR>
menu cp.r.1251 ;e ++enc=cp1251<CR>
menu cp.r.886  ;e ++enc=cp886<CR>

menu cp.w.utf8 ;set fenc=utf8<CR>
menu cp.w.1251 ;set fenc=cp1251<CR>
menu cp.w.886  ;set fenc=cp886<CR>

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 ПЛАГИНЫ                                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

syntax on
filetype plugin indent on

if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('$HOME/.vim/bundle')

Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'jlanzarotta/bufexplorer', {'on': 'BufExplorerHorizontalSplit'}
Plug 'powerman/vim-plugin-ruscmd'
Plug 'kien/ctrlp.vim'
Plug 'mileszs/ack.vim'

Plug 'tpope/vim-fugitive'

Plug '$HOME/.vim/bundle/visincr'
Plug 'tmhedberg/matchit'
Plug 'tpope/vim-repeat' | Plug 'tpope/vim-surround'
Plug 'mattn/emmet-vim', {'for': ['html', 'htmldjango', 'pug', 'jade']}

Plug '$HOME/.vim/bundle/php-mode'
Plug '2072/PHP-Indenting-for-VIm', {'dir': '$HOME/.vim/bundle/php-mode/bundle/PHP-Indenting-for-VIm'}
Plug 'shawncplus/phpcomplete.vim', {'dir': '$HOME/.vim/bundle/php-mode/bundle/phpcomplete'}
Plug 'rayburgemeestre/phpfolding.vim', {'dir': '$HOME/.vim/bundle/php-mode/bundle/phpfolding'}

Plug 'klen/python-mode', {'for': 'python', 'as': 'python-mode-devel', 'branch': 'develop' }

Plug 'chrisbra/csv.vim', {'for': 'csv'}

Plug 'digitaltoad/vim-pug', {'for': ['pug', 'jade']}
Plug 'peterhoeg/vim-qml', {'for': 'qml'}
Plug 'chr4/nginx.vim', {'for': 'nginx'}
Plug 'vim-scripts/SyntaxComplete'
Plug 'scrooloose/syntastic'
Plug 'groenewege/vim-less', {'for': ['less', 'css']}
if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    Plug 'zchee/deoplete-jedi'
endif
" Plug 'metakirby5/codi.vim'

Plug 'jonathanfilip/vim-lucius', {'dir': '$HOME/.vim/colors/vim-lucius'}
Plug  'christoomey/vim-tmux-navigator'

Plug 'editorconfig/editorconfig-vim'

Plug 'blindFS/vim-taskwarrior'

if has('nvim')
    function! BuildComposer(info)
        if a:info.status != 'unchanged' || a:info.force
            !cargo build --release
            UpdateRemotePlugins
        endif
    endfunction

    Plug 'euclio/vim-markdown-composer', { 'for': 'markdown', 'do': function('BuildComposer') }
endif




call plug#end()

"{{{ airline
"" устраняем задерку отображения переключения режимов в airline
set ttimeoutlen=50

let g:airline_left_sep = '▶'
let g:airline_right_sep = '◀'
silent let g:airline_symbols = {}
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'

let g:airline#extensions#whitespace#enabled = 0
"}}}

"{{{ ___NERDTree____
let g:Tlist_Show_One_File = 1

let NERDTreeIgnore = [
    \'\~$',
    \'\.DS_Store$',
    \'\.(git|hg|svn|ropeproject)$',
    \'__pycache__|migrations|bower_components|node_modules',
    \'\v\.(o|so|pyc|\~|jpg|jpe|jpeg|png|wav|mp3|ogg|pdf)$',
\]

" где храним закладки NERDTree
let NERDTreeBookmarksFile=expand("$HOME/.vim_local/.NERDTreeBookmarks")

" автоматическое открытие панели, если не выбран файл
"autocmd vimenter * if !argc() | NERDTree | endif
"}}}

"#{{{ CTRL-P
let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
            \ --ignore .git
            \ --ignore .hg
            \ --ignore .svn
            \ --ignore .ropeproject
            \ --ignore __pycache__
            \ --ignore migrations
            \ --ignore node_modules
            \ --ignore "**/*.o"
            \ --ignore "**/*.so"
            \ --ignore "**/*.pyc"
            \ -g ""'

let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/](\.(git|hg|svn|ropeproject)|(__pycache__|migrations|node_modules))$',
    \ 'file': '\v\.(o|so|pyc|\~|jpg|jpe|jpeg|png|wav|mp3|ogg)$',
    \ 'link': 'some_bad_symbolic_links',
    \ }


" релокация кеша ctrlp
let g:ctrlp_cache_dir = '~/.vim_local/ctrlp'
if !isdirectory(expand(g:ctrlp_cache_dir))
    call mkdir(expand(g:ctrlp_cache_dir), "p")
endif
"}}}

"#{{{ ACK
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
"}}}

"{{{ ___Repeat______
" регистрируем vim-repeat
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)
"}}}

"{{{ DEOPLETE
if has('nvim')
    let g:deoplete#enable_at_startup = 1
    let g:deoplete#enable_smart_case = 1
    let g:deoplete#file#enable_buffer_path=1
    let g:deoplete#auto_completion_start_length = 0
endif
"}}}

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 MAP & COMMAND                                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{
" переназначем command-mode
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;
cnoremap ; :
cnoremap : ;
inoremap ; :
inoremap : ;

" переназначем <leader>
nnoremap <Space> <Nop>
let mapleader="\<Space>"

" выполняем внешнюю команду
nnoremap ! :!

" удоная навигация при set wrap
map k gk
map j gj

" блокируем активацию режима замены по двойному INS
imap <Ins> <Esc>li
" выходим из режима вставки по ESC
imap jj <Esc>

" сброс подсветки поисковой фразы
nmap <silent> <Leader>h ;nohlsearch<CR>
" отображение непечатных символов
nmap <silent> <Leader>l ;set list!<CR>
" отображаем линию по всей текущей колонке
nmap <silent> <Leader>cc ;set cursorcolumn!<CR>
" чистим trailind-spaces
nmap <silent> <Leader>x<Space> ;%s/\s\+$//i<CR><CR>'' zz

" НАВИГАЦИЯ ПО ОКНАМ {{{
" верт. сплит
nnoremap <Leader>v <C-w>v<C-l>
nnoremap <Leader>s <C-w>s

" сама навигация
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" увеличиваем размер окна по горизонтали
nnoremap <C-v>v     ;vertical resize +5<CR>
nnoremap <C-v><C-v> ;vertical resize +5<CR>
nnoremap <C-w>m     <C-w><C-_><C-w><C-\|>
nnoremap <C-w><C-m> <C-w><C-_><C-w><C-\|>

" двигаем табы
nmap <silent> <Leader>> ;tabmove +1<CR>
nmap <silent> <Leader>< ;tabmove -1<CR>
"}}}

" навигация по буфферам
nmap <silent> <Leader>] ;bnext<CR>
nmap <silent> <Leader>bd ;bdelete<CR>
nmap <silent> <Leader>[ ;bprevious<CR>

" веставляем рабочий каталог на текущий буффер
nnoremap <Leader>cd :cd %:p:h<CR>:pwd<CR>
" удобное сохранение всех изменённых файлов
nmap __ ;wa<CR>
nmap gzz ;wqa<CR>
nmap gzq ;qa!<CR>
" сохранение с правами root
cmap w!! w !sudo tee > /dev/null %


"{{{ РЕМАП ДЕФОЛТНЫХ СОЧЕТАНИЙ
" перемещаемся по визуальным строкам, а не реальным
map j gj
map k gk

" выставляем логику Y как у D, C, ... и т.д
nnoremap Y y$

" при поиске не прыгать на следующее найденное, а просто подсветить
nnoremap * *N
nnoremap # #N
" в визуальном режиме по команде * подсвечивать выделение
vnoremap * y :execute ":let @/=@\""<CR> :execute "set hlsearch"<CR>
"TODO : g*, #, g#

" выделение последнего вставленного текст
nnoremap gV `[v`]

" сохраняем выделение блоков при изменении отсутупов
vmap < <gv
vmap > >gv
"}}}

" copy&paste
vnoremap <S-Del>    "+x
vnoremap <C-Insert> "+y
map      <S-Insert> "+gP
cmap     <S-Insert> <C-R>+

" F5 - NERDTree
map  <F5> ;NERDTreeToggle<cr>
vmap <F5> <esc>;NERDTreeToggle<cr>
imap <F5> <esc>;NERDTreeToggle<cr>

" F6 - BuffExplorer
map  <F6> ;BufExplorerHorizontalSplit<cr>
vmap <F6> <esc>;BufExplorerHorizontalSplit<cr>
imap <F6> <esc>;BufExplorerHorizontalSplit<cr>

"{{{ view
if &readonly
    noremap <esc><esc> ;qa!<CR>
endif
"}}}
"{{{ diff
if &diff
    " space и s-space для хождения по изменениям
    noremap <space> ]cz
    noremap <S-space> [cz
    " <leader>g - :diffget изменения, <leader>p - :diffput
    noremap <leader>o ;diffget<CR>
    noremap <leader>p ;diffput<CR>
endif
"}}}

"{{{ COMMANDS
" переприменяем syntax раскраску
command! ReSyntax syntax sync fromstart

" скопировать все результаты поиска в регистр "a
function! DoCopySearchResults()
    let hits = []
    %s//\=len(add(hits, submatch(0))) ? submatch(0) : ''/ge
    let @a = join(hits, "\n")
endfunction
command! CopySearchResults call DoCopySearchResults()
"}}}

"{{{ ОЧЕПЯТКИ
"TODO : использовать не только команды, юно и абревиатуры
command! W w
command! Q q
command! Qa qa
command! QA qa
command! Qw wq
"}}}

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 FILETYPE настройки                                           "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     GIT                                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

autocmd FileType gitcommit :setlocal colorcolumn=80
autocmd FileType gitcommit :setlocal spelllang=ru_yo,en_us
autocmd FileType gitcommit :setlocal spell

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     MARKDOWN                                                 "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{
" vim-markdown-composer

let g:markdown_composer_syntax_theme = 'github'
let g:markdown_composer_autostart = 1
let g:markdown_composer_open_browser = 1

autocmd FileType markdown nmap <buffer> <silent> <leader>r ;ComposerStart<CR>
autocmd FileType markdown silent setlocal spell spelllang=ru_yo,en_us
"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     XML                                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

" форматирование xml документа
function! DoPrettyXML()
    1,$!xmllint --format --recover --encode utf-8 -
endfunction
function! DoPrettyHTML()
    1,$!tidy -indent -ashtml -wrap 0 -utf8 -
endfunction
function DoPrettyJSON()
    :%!python3 -c "import json, sys; print(json.dumps(json.load(sys.stdin), indent=4, ensure_ascii=False))"
endfunction
function DoPrettyJS()
    :%!python3 -c "import jsbeautifier, sys; print(jsbeautifier.beautify(sys.stdin.read()))"
endfunction

command! PrettyXML call DoPrettyXML()
command! PrettyHTML call DoPrettyHTML()
command! PrettyJS call DoPrettyJS()
command! PrettyJSON call DoPrettyJSON()

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     CONFIG                                                   "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

" ставим фолдинги по маркеру для конфигов
autocmd FileType conf setlocal foldmethod=marker

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     BASH                                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

autocmd FileType sh setlocal makeprg=/bin/bash\ %

if has("autocmd")
    augroup content
        autocmd BufNewFile *.sh
           \ 0put = '#!/bin/bash'  |
           \ 1put = '# -*- coding: utf-8 -*-' |
           \ $put = '' |
           \ $put = '' |
    "      \ $put = '# vim: set sw==3 tw=80 :' |
           \ normal gg19jf]
           \ normal G
        autocmd BufWritePost *.sh silent! !chmod +x %
    augroup END
endif

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     PHP                                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     QML                                                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

autocmd BufRead,BufNewFile *.qml setlocal filetype=qml
autocmd FileType qml setlocal makeprg=/usr/bin/qmlviewer\ %

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     HTML                                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{
function! Jade2Html() range
    " конвертация разметки html в jade для визуального режима
    let l:indent = indent(a:firstline)/shiftwidth()

    execute a:firstline . "," . a:lastline . "s/^\\s\\{" . indent(a:firstline) . "}//"
    execute a:firstline . "," . a:lastline . "!jade --pretty"

    let c = 0
    let spaces = " "
    while c <= shiftwidth()
        let spaces .= " "
        let c = c + 1
    endwhile
    execute "'[,']s/\\s\\{2}/" . spaces . "/g"

    execute "normal '[V']" . l:indent . ">gv"
endfunction

autocmd FileType pug xmap <buffer> <silent> <leader>h ;call Jade2Html()<CR>
autocmd FileType html xmap <buffer> <silent> <leader>h ;call Jade2Html()<CR>
autocmd FileType htmldjango xmap <buffer> <silent> <leader>h ;call Jade2Html()<CR>
"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     JADE                                                     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{
function! Html2Jade() range
    " конвертация разметки html в jade для визуального режима
    let l:indent = indent(a:firstline)/shiftwidth()

    execute a:firstline . "," . a:lastline . "!html2jade --donotencode --bodyless --noemptypipe --nspaces " &shiftwidth
    execute "normal '[V']" . l:indent . ">gv"
endfunction

autocmd BufNewFile,BufRead *.jade set filetype=pug
autocmd BufNewFile,BufRead *.pug set filetype=pug

autocmd FileType pug xmap <buffer> <silent> <leader>j ;call Html2Jade()<CR>
autocmd FileType html xmap <buffer> <silent> <leader>j ;call Html2Jade()<CR>
autocmd FileType htmldjango xmap <buffer> <silent> <leader>j ;call Html2Jade()<CR>
"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     PYTHON                                                   "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

" TODO: отрефакторить этот блок
autocmd FileType *
    \ if &omnifunc == "" |
    \     set omnifunc=syntaxcomplete#Complete |
    \ endif

"В .py файлах включаем умные отступы после ключевых слов
autocmd BufRead *.py setlocal smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class

if has("autocmd")
    augroup content
        autocmd BufNewFile *.py
           \ 0put = '#!/usr/bin/env python'  |
           \ 1put = '# -*- coding: utf-8 -*-' |
           \ $put = '' |
           \ $put = '' |
    "      \ $put = '# vim: set sw==3 tw=80 :' |
           \ normal gg19jf]
           \ normal G
        " autocmd BufWritePost *.py silent! !chmod +x %
    augroup END
endif


let g:pymode_rope = 1                       " Включаем библиотеку Rope
let pymode_lint_ignore = ["E501", "E128", "E266"]   " PEP8 относительно размера строк
if has('nvim')
    let g:pymode_rope_completion = 1            " использовать дополнение из Rope
    let g:pymode_rope_complete_on_dot = 1       " использовать автодополнение после .
    let g:pymode_rope_completion_bind = '<C-Space>'
else
    let g:pymode_rope_completion = 0
endif

let g:pymode_syntax = 1                     " продвинутая раскраска от Rope
let g:pymode_rope_autoimport = 1
let g:pymode_syntax_all = 1                 " красим всё
let g:pymode_folding = 0                    " отключаем фолдинг
let g:pymode_python = 'python3'


" проверка кода в соответствии с PEP8 через <leader>8
autocmd FileType python map <buffer> <leader>8 :PymodeLint<CR>

"отключаем syntastic
let g:syntastic_ignore_files = ['\.py$']
" или можно отключить проверку синтаксиса в python-mode
"let g:pymode_lint = 0

" surround для django
augroup django_template_tags
    autocmd!
    autocmd Bufread *.{html,jade,pug} let g:surround_{char2nr("%")} = "{% \r %}"
    autocmd Bufread *.{html,jade,pug} let g:surround_{char2nr("s")} = "{% static \"\r\" %}"
    autocmd Bufread *.{html,jade,pug} let g:surround_{char2nr("{")} = "{{ \r }}"
    autocmd Bufread *.{html,jade,pug} let g:surround_{char2nr("}")} = "{{ \r }}"
    autocmd Bufread *.{jade,pug}      let g:surround_{char2nr("#")} = "#{\r}"
augroup END

autocmd FileType python iabbrev utf! # -*- coding; utf-8 -*-
autocmd FileType python iabbrev mdl! # vim; foldmethod=marker foldcolumn=4 foldlevel=0 textwidth=120 fileencoding=utf-8
"}}}

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 КОСТЫЛЬ ДЛЯ РУССКОЙ РАСКЛАДКИ                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{
" Львиная доля маппингов покрыта плагином ruscmd

map №          #
map в;         d$

" surround-vim
map нышц       ysiw
map нышЦ       ysiW
map ныфц       ysaw
map ныфЦ       ysaW
map вые        dst

" мои маппинги
map <leader>ы         <leader>s
map <leader>м         <leader>v
map <leader>д         <leader>l
map <leader>ч<SPACE>  <leader>x<SPACE>
map <C-w><C-ь> <C-w><C-m>
map <C-w>ь     <C-w>m

" TODO:
"       f/F/t/T '/"/@/#/...
"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 ПОДГРУЗКА ЛОКАЛЬНЫХ КОНФИГОВ                                 "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{

let g:vimlocal_root = glob("$HOME/.vim_local/")
let g:vimlocal_rc = glob("$HOME/.vimrc_local")

if !empty(g:vimlocal_rc)
    let s:pathogen = g:vimlocal_root . 'autoload/pathogen.vim'
    if filereadable(s:pathogen)
        execute 'source ' . fnameescape(s:pathogen)
    endif

    execute 'source ' . fnameescape(g:vimlocal_rc)
endif

" Запрещаем autocmd из-за соображений безопастности
"   т.к. все конфиги загружены, настройка применется лишь к внешним .exrc
"   см. `set exrc` в начеле конфига
"set secure

"}}}

" NEOVIM {{{
if has('nvim')
    " python {{{
    " не забыть `sudo pip install -U neovim`
    let g:loaded_python_provider=1
    " let g:python_host_prog = '/usr/bin/python2'
    let g:python3_host_prog = '/usr/bin/python3'
    " }}}

    " настройка терминала {{{
    tnoremap <Esc> <C-\><C-n>
    " навигация по ^{h,j,k,l}
    tnoremap <C-h> <C-\><C-n><C-w>h
    tnoremap <C-j> <C-\><C-n><C-w>j
    tnoremap <C-k> <C-\><C-n><C-w>k
    tnoremap <C-l> <C-\><C-n><C-w>l
    nnoremap <C-h> <C-w>h
    nnoremap <C-j> <C-w>j
    nnoremap <C-k> <C-w>k
    nnoremap <C-l> <C-w>l
    " }}}
endif
" }}}
