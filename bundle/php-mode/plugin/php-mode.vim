""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 							БАЗОВЫЕ НАСТРОЙКИ	                               "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

autocmd FileType php set keywordprg=$HOME/.vim/bundle/php-mode/bin/keywordprg

set makeprg=php\ -l\ %                                  " Проверка синтаксиса PHP
set errorformat=%m\ in\ %f\ on\ line\ %l                " Формат вывода ошибок PHP

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 							ПЛАГИНЫ    	                        		       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" folding plugin
autocmd FileType php setlocal foldmethod=manual
" загружаем сам плагин
" if you can't use the after directory in step 3
let php_folding=0
" if you're not using the default plugin directory
autocmd FileType php :source $HOME/.vim/bundle/php-mode/ftplugin/phpfolding.vim

" indent plugin
let g:PHP_removeCRwhenUnix = 1
let g:PHP_vintage_case_default_indent = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 							ХОТКЕИ     	                        		       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <F9>  <Esc>:EnableFastPHPFolds<Cr>
map <F10> <Esc>:DisablePHPFolds<Cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 							ОТОБРАЖЕНИЕ	                        		       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

autocmd FileType php let php_folding = 1                " Включаем фолдинг для блоков классов/функций
autocmd FileType php let php_noShortTags = 0            " Не использовать короткие теги PHP для поиска PHP блоков
autocmd FileType php let php_baselib = 1                " подстветка базовых функций PHP
autocmd FileType php let php_sql_query = 1	            " Подстветка SQL внутри PHP строк
autocmd FileType php let php_htmlInStrings = 1	        " Подстветка HTML внутри PHP строк


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 							ШАБЛОНЫ		                                       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Вывод отладочной информации
iabbrev tm echo 'Test message in file: '.__FILE__.', on line: '.__LINE__;
